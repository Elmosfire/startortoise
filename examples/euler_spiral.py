from startortoise import turtle_to_mp4, TurtleCommand, logaritmic_buildoff
from math import pi
import logging



for x in [logging.getLogger(name) for name in logging.root.manager.loggerDict]:
    if x.name.startswith("startortoise"):
        x.setLevel(logging.DEBUG)

print([logging.getLogger(name) for name in logging.root.manager.loggerDict])

def euler_spiral(iterations, base):
    for i in range(iterations):
        yield TurtleCommand.forward(20)
        yield TurtleCommand("left", base * i)


def euler_spiral_save(iterations, base):
    turtle_to_mp4(euler_spiral(iterations, base), "background.jpg", "euler.mp4", logaritmic_buildoff(100))

euler_spiral_save(10**5, 0.0483394 / pi * 180)