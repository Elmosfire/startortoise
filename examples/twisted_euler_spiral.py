from startortoise import turtle_to_mp4, TurtleCommand, logaritmic_buildoff, turnlog, build_video_cairo
from math import pi
import logging

from functools import reduce
from itertools import islice, count

def combine(a, b, mx):
    a = list(a)
    for x in b:
        for y in a:
            yield x*y % mx
            
def mult(a, rep, mx):
    r = [1]
    for _ in range(rep):
        r = combine(a, r, mx)
    return r

def base(a, mx):
    a = list(a)
    b = list(a)
    for _ in range(100):
        b = list(combine(a, b, mx))
        if b[0] == 1:
            return b
    else:
        assert False, "not able to build base after 100 tries"

def freemult(a, mx):
    r = [1]
    a = base(a, mx)
    past = []
    while True:
        yield from r
        past.extend(r)
        r = list(combine(past, a[1:], mx))
        
def twisted_spiral(part, it, px=2):
    core = [1] + [x for x in range(1,part) if 2*x != part]
    print(core)
    assert sum(core) % part == 1, "invalid core"
    return turnlog(islice(freemult(core, part), it), part, px)

def twisted_spiral_2(part, it, px=2):

    return turnlog((i%part for i in range(it)), part, px)

def ftwist(base):
    for x in count(1):
        y = x
        while y%base == 0:
            y = y//base
        yield (y + base//2)%base
        
def unpack(n, base):
    while n > 0:
        yield n%base
        n = n//base
    yield 0
        

        
def ftwist2(base):
    pool = 0
    yield 0
    for x in count(1):
        y = unpack(x, base)
        
        while True:
            f1 = next(y)
            if f1:
                f2 = next(y)
                break
        
        
        if f1+f2 >= base:
            yield (x + base//2 + 1)%base
        else:
            yield (x + base//2)%base
        
#print(list(islice(ftwist2(5), 50)))

print(list(unpack(120, 5)))

turtle_to_mp4(islice(turnlog(ftwist2(61),60, 20), 60**5), "background.jpg", "euler_twisted_61.mp4", logaritmic_buildoff(120))