"""
A package to convert a turtle path to an animated mp4
"""

from .svgToMp4 import build_video, white_slate, build_video_cairo
from .turtle_instrv import turtle_to_mp4, turtle_to_svg_iter, TurtleCommand, logaritmic_buildoff, turnlog

__version__ = "0.1.0"
__author__ = "Thorvald Dox"


