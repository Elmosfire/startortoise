from pathlib import Path
import string
from PIL import Image
import subprocess as sp
import numpy as np
from itertools import islice
from time import time
from tempfile import TemporaryDirectory
from lxml import etree
import io
import tqdm
import logging
from cairosvg import svg2png

import asyncio

logger = logging.getLogger(__name__)

async def run(cmd, cwd):
    proc = await asyncio.create_subprocess_shell(
        cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE, cwd=cwd
    )

    stdout, stderr = await proc.communicate()


async def build_frame(root, x, crop, w, h, rep):
    logger.debug("writing to " + str(root))
    with (root / "raw.svg").open("w") as file:
        file.write(x)

    if crop:
        await run(
            (
                "inkscape ./raw.svg --export-type=svg --export-area-drawing -o ./trimmed.svg"
            ),
            cwd=root,
        )
        fn = root / "trimmed.svg"
    else:
        fn = root / "raw.svg"
    with fn.open() as file:
        tree = etree.parse(file)
        xroot = tree.getroot()
        rw = xroot.get("width")
        rh = xroot.get("height")
        mxh = str(max(float(rw) * h / w, float(rh)))
        mxw = str(float(mxh) * w / h)
        xroot.set("viewBox", f"0 0 {mxw} {mxh}")
        xroot.set("width", mxw)
        xroot.set("height", mxh)
        xroot.set("style", "background-color:green")
        tree.write(str(root / "boxed.svg"))
    await run(f"inkscape boxed.svg -w {w} -h {h} -o boxed.png", cwd=root)
    return root, rep


@asyncio.coroutine
def write_frames(pipe, tasks, bg=None):
    for task in tqdm.tqdm(tasks, desc="writing frames"):
        result, rep = yield from task

        im = Image.open(str(result / "boxed.png"))
        im2 = np.array(im)
        if bg is not None:
            im3 = im2[:, :, :3] + (255 - im2[:, :, 3, None]) // 255 * (bg // 2 + 127)
        else:
            im3 = im2[:, :, :3] + (255 - im2[:, :, 3, None])
        crs = im3.tobytes()
        for _ in range(rep):
            pipe.stdin.write(crs)
            
parser = etree.XMLParser(huge_tree=True)
def sync_frame(root, x, w, h, rep, pipe, bg):
    logger.debug("writing to " + str(root))
    
    xroot = etree.fromstring(x, parser=parser)
    rw = float(xroot.get("width"))
    rh = float(xroot.get("height"))
    #assert rw/w == rh/h, f"aspect ratio mismatch {w}/{rw}, {h}/{rh}"
    

    im = Image.open(io.BytesIO(svg2png(bytestring=x.decode("utf-8"), write_to=None, scale=w/rw)))    
    

    im2 = np.array(im.resize((w,h)))
    #im.save("tmp.png")
    #assert (im2.shape[2] == 4), im2.shape
    if bg is not None:
        im3 = im2[:, :, :3] + (255 - im2[:, :, 3, None]) // 255 * (bg // 2 + 127)
    else:
        im3 = im2[:, :, :3] + (255 - im2[:, :, 3, None])
    crs = im3.tobytes()
    for _ in range(rep):
        pipe.stdin.write(crs)


def build_chunks(generator):
    while True:
        chunk = list(islice(generator, 100))
        if chunk:
            yield chunk
        else:
            break


def white_slate(w, h):
    return np.ones([h, w, 3], dtype=np.int) * 255


def build_video(generator_, bg, filename, fps=30, crop=False):
    logger.debug("start video build " + str(filename))
    if isinstance(bg, str):
        bg = np.array(Image.open(bg))[:, :, :3]
    elif isinstance(bg, Path):
        bg = np.array(Image.open(str(bg)))[:, :, :3]
    elif isinstance(bg, io.IOBase):
        bg = np.array(Image.open(bg))[:, :, :3]

    h, w, _ = bg.shape
    
    assert not Path(filename).exists(), f"file {filename} already exists"

    cmd_out = [
        "ffmpeg",
        "-f",
        "rawvideo",
        "-vcodec",
        "rawvideo",
        "-s",
        f"{w}x{h}",
        "-pix_fmt",
        "rgb24",
        "-r",
        f"{fps}",
        "-i",
        "-",
        "-f",
        "flv",
        "-vcodec",
        "libx264",
        "-profile:v",
        "main",
        "-g",
        "60",
        "-keyint_min",
        "30",
        "-pix_fmt",
        "yuv420p",
        "-preset",
        "ultrafast",
        "-tune",
        "zerolatency",
        filename,
    ]

    pipe = sp.Popen(cmd_out, stdin=sp.PIPE)

    try:
        loop = asyncio.get_event_loop()
        for chunk in build_chunks(enumerate(tqdm.tqdm(generator_, desc="building chunks"))):
            with TemporaryDirectory() as dir:
                mroot = Path(dir)
                tasks = []
                for i, x in chunk:
                    root = mroot / str(i)
                    root.mkdir()
                    if isinstance(x, tuple):
                        x, rep = x
                    else:
                        rep = 1
                    task = loop.create_task(build_frame(root, x, crop, w, h, rep))
                    tasks.append(task)

                loop.run_until_complete(write_frames(pipe, tasks, bg))

    finally:
        pipe.stdin.close()
        pipe.wait()

        # Make sure all went well
        if pipe.returncode != 0:
            raise sp.CalledProcessError(pipe.returncode, cmd_out)


def build_video_cairo(generator_, bg, filename, fps=30):
    logger.debug("start video build " + str(filename))
    if isinstance(bg, str):
        bg = np.array(Image.open(bg))[:, :, :3]
    elif isinstance(bg, Path):
        bg = np.array(Image.open(str(bg)))[:, :, :3]
    elif isinstance(bg, io.IOBase):
        bg = np.array(Image.open(bg))[:, :, :3]

    h, w, _ = bg.shape
    
    if Path(filename).exists():
        Path(filename).unlink()

    cmd_out = [
        "ffmpeg",
        "-f",
        "rawvideo",
        "-vcodec",
        "rawvideo",
        "-s",
        f"{w}x{h}",
        "-pix_fmt",
        "rgb24",
        "-r",
        f"{fps}",
        "-i",
        "-",
        "-f",
        "flv",
        "-vcodec",
        "libx264",
        "-profile:v",
        "main",
        "-g",
        "60",
        "-keyint_min",
        "30",
        "-pix_fmt",
        "yuv420p",
        "-preset",
        "ultrafast",
        "-tune",
        "zerolatency",
        filename,
    ]

    pipe = sp.Popen(cmd_out, stdin=sp.PIPE)

    try:
        for  i, x in enumerate(tqdm.tqdm(generator_, desc="building chunks")):
            with TemporaryDirectory() as dir:
                mroot = Path(dir)
                root = mroot / str(i)
                root.mkdir()
                if isinstance(x, tuple):
                    x, rep = x
                else:
                    rep = 1
                sync_frame(root, x, w, h, rep, pipe, bg)




    finally:
        pipe.stdin.close()
        pipe.wait()

        # Make sure all went well
        if pipe.returncode != 0:
            raise sp.CalledProcessError(pipe.returncode, cmd_out)