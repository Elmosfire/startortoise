from pickle import NONE
from re import L
import subprocess
from itertools import count
from functools import reduce
from math import sqrt, pi
import logging
from pathlib import Path
from tqdm import tqdm
from lxml import etree

from svg_turtle import SvgTurtle
from .svgToMp4 import build_video_cairo


logger = logging.getLogger(__name__)


def compound(*funcs):
    def compound2(inner_func, outer_func):
        def wrapper(*args, **kwargs):
            return outer_func(inner_func(*args, **kwargs))

        return wrapper

    return reduce(compound2, funcs)


class TurtleCommand:
    screensize = 400

    def __init__(self, type_, value):
        self.type_ = type_
        self.value = value

    def sub(self, scale):
        return TurtleCommand(self.type_, self.value * scale)

    def __call__(self, t):
        logger.debug(self.type_, self.value)
        if self.type_ == "left":
            t.left(self.value)
        elif self.type_ == "right":
            t.right(self.value)
        elif self.type_ == "forward":
            t.forward(self.value)
    
    @classmethod
    def left(cls, angle):
        return cls("left", angle)

    @classmethod
    def right(cls, angle):
        return cls("right", angle)

    @classmethod
    def forward(cls, pixels):
        return cls("forward", pixels)

    def __repr__(self):
        return f"{self.type_}-{self.value}"
    
    
class TurtleWrapper:
    mxsz=(100_000, 100_000)
    parser = etree.XMLParser(huge_tree=True)
    def __init__(self):
        self.turtle = SvgTurtle(*self.mxsz)

    def execute(self, command):
        command(self.turtle)
        
    def svg(self, res):
        xroot = etree.fromstring(self.turtle.to_svg(), parser=self.parser)
        
        l, r, t, b = None, None, None, None
        for p in xroot:
            if not p.tag.endswith("polyline"):
                continue
            for p0 in p.get("points").split(" "):
                x,y = map(float, p0.split(","))
                if l is None or l < x:
                    l = x
                if r is None or r > x:
                    r = x
                if t is None or t < y:
                    t = y
                if b is None or b > y:
                    b = y    
        
        x = (l+r)/2
        y = (t+b)/2
        w = (r-l)
        h = (t-b)
        rw = max(w, h*res, 8)
        rh = rw/res
        l = x-rw/2
        r = x+rw/2
        t = y-rh/2
        b = y+rh/2


        xroot.set("viewBox", f"{l} {t} {r-l} {b-t}")
        xroot.set("width", str(r-l))
        xroot.set("height", str(b-t))
        #xroot.set("width", "100")
        #xroot.set("height", "100")
        with Path("tmp.svg").open("w") as file:
            file.write(etree.tostring(xroot).decode("utf-8"))
        
        return etree.tostring(xroot)
        


class LogaritmicBuildOff:
    store = {}

    def __init__(self, base):
        self.base = base
        self.current = 0
        self.history = []
        self.store[base] = self

    def increase_until(self, value):
        while self.current <= value:
            self.current += self.current // self.base + 1
            self.history.append(self.current)

    def __contains__(self, value):
        self.increase_until(value)
        return value in self.history


def logaritmic_buildoff(base=100):
    if base not in LogaritmicBuildOff.store:
        LogaritmicBuildOff(base)
    return LogaritmicBuildOff.store[base]


def turnlog(instr, fillcircle, fincr):
    for x in tqdm(instr, desc="instuctions"):
        yield TurtleCommand.left(x/fillcircle * 360)
        yield TurtleCommand.forward(fincr)

def turtle_to_svg_iter(instructions, buildoff=None, endscreen=120):
    t = TurtleWrapper()
    i = 0
    try:
        with tqdm(desc="turtle command iterator") as pbar:
            for x in instructions:
                t.execute(x)
                if x.type_ == "forward":
                    i += 1
                    if buildoff is None or i in buildoff:
                        pbar.update(1)
                        yield t.svg(1920/1080)
    finally:
        if endscreen:
            yield t.svg(1920/1080), endscreen


def turtle_to_mp4(instructions, bg, out, buildoff=None, *, mxsz=(100_000, 100_000), endscreen=120, fps=30):
    it = turtle_to_svg_iter(instructions, buildoff, endscreen)
    
    build_video_cairo(it, bg, out, fps)




